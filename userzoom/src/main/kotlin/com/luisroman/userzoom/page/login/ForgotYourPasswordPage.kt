package com.luisroman.userzoom.page.login

import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
open class ForgotYourPasswordPage: LoginPage() {

    @PostConstruct
    open fun initForgotYourPassword() = initializePageFactory(this)

    fun isForgotPasswordTitleDisplayed(): Boolean = getForgotPasswordTitle.isDisplayed

    fun isChangePasswordEmailWasSuccessfulAlert(): Boolean {
        elementUtils.waitForClickable(getChangePasswordEmailWasSuccessfulAlert)
        return getChangePasswordEmailWasSuccessfulAlert.isDisplayed
    }

    fun sendEmailToGetPassword(email: String) {
        isForgotPasswordTitleDisplayed()
        fillEmailInput(email)
        clickSubmitButton()
    }

    @FindBy(xpath = "//div[@title='$FORGOT_PASSWORD_TITLE']")
    private lateinit var getForgotPasswordTitle: WebElement

    @FindBy(xpath = "//*/span/span[text()=\"$EMAIL_SENT_TEXT\"]")
    private lateinit var getChangePasswordEmailWasSuccessfulAlert: WebElement

    companion object {
        private const val FORGOT_PASSWORD_TITLE = "Forgot your password?"
        private const val EMAIL_SENT_TEXT = "We've just sent you an email to reset your password."
    }

}