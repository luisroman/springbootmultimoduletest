package com.luisroman.userzoom.page

import com.luisroman.seleniumbase.utils.ElementUtils
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.PageFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
open class BasePage {

    @Autowired
    protected lateinit var driver: WebDriver

    @Autowired
    protected lateinit var elementUtils: ElementUtils

    @Value("\${url.mainurl}")
    protected lateinit var mainUrl: String

    protected fun initializePageFactory(page: Any) = PageFactory.initElements(driver, page)

}