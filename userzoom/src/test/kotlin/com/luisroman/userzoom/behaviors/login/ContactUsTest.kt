package com.luisroman.userzoom.behaviors.login

import com.luisroman.seleniumbase.utils.SeleniumTest
import com.luisroman.userzoom.page.login.LoginPage
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.openqa.selenium.WebDriver
import java.util.ArrayList
import com.github.romankh3.image.comparison.ImageComparisonUtil.readImageFromResources
import com.github.romankh3.image.comparison.model.Rectangle
import com.github.romankh3.image.comparison.ImageComparison
import com.github.romankh3.image.comparison.model.ComparisonState.MATCH
import org.apache.commons.io.FileUtils
import org.junit.jupiter.api.Assertions.assertEquals
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot
import java.io.File
import javax.imageio.ImageIO
import kotlin.math.absoluteValue


@SeleniumTest
@RunWith(JUnitPlatform::class)
@ExtendWith(SpringExtension::class)
@DisplayName("Contact Us Test")
open class ContactUsTest {

    @Autowired
    lateinit var loginPage: LoginPage

    @Autowired
    lateinit var driver: WebDriver


    @Test
    fun `Contact Us link is displayed with proper Href`() {
        assertTrue(loginPage.isContactUsLinkWithProperHrefDisplayed())
        Thread.sleep(2000L)
        val scrFile = (driver as TakesScreenshot).getScreenshotAs<File>(OutputType.FILE)
        val expected = readImageFromResources("testfile.png")
        //val actual = readImageFromResources("testfile2.png")
        val actual = ImageIO.read(scrFile)
        val excludedAreas = ArrayList<Rectangle>()
        excludedAreas.add(Rectangle(1362, 0, 2048, 1290))
        val imageComparison = ImageComparison(expected, actual).setExcludedAreas(excludedAreas)

        //when
        val result = imageComparison.compareImages()

        //then
        assertEquals(result.comparisonState, MATCH)


    }

}