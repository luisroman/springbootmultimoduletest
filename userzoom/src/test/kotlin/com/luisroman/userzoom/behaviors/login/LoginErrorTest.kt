package com.luisroman.userzoom.behaviors.login

import com.luisroman.userzoom.page.login.LoginPage
import com.luisroman.seleniumbase.utils.SeleniumTest
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.junit.jupiter.SpringExtension

@SeleniumTest
@RunWith(JUnitPlatform::class)
@ExtendWith(SpringExtension::class)
@DisplayName("Login Error Test")
open class LoginErrorTest {

    @Autowired
    lateinit var loginPage: LoginPage

    private val userEmail = "testing${System.currentTimeMillis()}@gmail.com"

    @Test
    fun `Password required alert is displayed`() {
        loginPage.login(userEmail, "")
        assertTrue(loginPage.isPasswordRequiredAlertDisplayed())
    }

    @Test
    fun `Email required alert is displayed`() {
        loginPage.login("", PASSWORD)
        assertTrue(loginPage.isEmailRequiredAlertDisplayed())
    }

    @Test
    fun `Email and Password required alert are displayed`() {
        loginPage.login("", "")
        assertTrue(loginPage.isEmailRequiredAlertDisplayed())
        assertTrue(loginPage.isPasswordRequiredAlertDisplayed())
    }

    @Test
    fun `Email requires @ Alert is displayed`() {
        loginPage.login(EMAIL_WITHOUT_AT, PASSWORD)
        assertTrue(loginPage.isEmailInvalid())
    }

    @Test
    fun `User is not found Alert is displayed`() {
        loginPage.login(userEmail, PASSWORD)
        assertTrue(loginPage.isUserNotFoundAlertDisplayed())
    }

    @Test
    fun `User is blocked after 10 tries`() {
        val blockEmail = "testing${System.currentTimeMillis()}@gmail.com"
        loginPage.tryLoginXTimes(blockEmail, PASSWORD, 10)
        assertTrue(loginPage.isUserBlockedAlertDisplayed())
    }

    companion object {
        private const val EMAIL_WITHOUT_AT = "testhotmail.com"
        private const val PASSWORD = "Password1234"
    }

}