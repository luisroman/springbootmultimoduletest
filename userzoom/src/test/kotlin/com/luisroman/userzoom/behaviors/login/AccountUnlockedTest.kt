package com.luisroman.userzoom.behaviors.login

import com.luisroman.userzoom.page.login.LoginPage
import com.luisroman.seleniumbase.utils.SeleniumTest
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.junit.jupiter.SpringExtension

@SeleniumTest
@RunWith(JUnitPlatform::class)
@ExtendWith(SpringExtension::class)
@DisplayName("Account Unlocked Test")
open class AccountUnlockedTest {

    @Autowired
    lateinit var loginPage: LoginPage

    @Test
    fun `Account unlocked message is displayed`() {
        loginPage.openWebWithUnlockedAccountMessage()
        assertTrue(loginPage.isUserUnlockedMessageDisplayed())
    }

}