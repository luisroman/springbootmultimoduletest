package com.luisroman.userzoom.behaviors.login

import com.luisroman.userzoom.page.login.ForgotYourPasswordPage
import com.luisroman.userzoom.page.login.LoginPage
import com.luisroman.seleniumbase.utils.SeleniumTest
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.junit.jupiter.SpringExtension

@SeleniumTest
@RunWith(JUnitPlatform::class)
@ExtendWith(SpringExtension::class)
@DisplayName("Forgot Password Test")
open class ForgotPasswordTest {

    @Autowired
    lateinit var loginPage: LoginPage

    @Autowired
    lateinit var forgotYourPasswordPage: ForgotYourPasswordPage

    @Test
    fun `Change password email is successfully sent`() {
        loginPage.clickForgotPasswordLink()
        forgotYourPasswordPage.sendEmailToGetPassword(EMAIL)
        assertTrue(forgotYourPasswordPage.isChangePasswordEmailWasSuccessfulAlert())
    }

    companion object {
        private const val EMAIL = "1654@hotmail.com"
    }

}