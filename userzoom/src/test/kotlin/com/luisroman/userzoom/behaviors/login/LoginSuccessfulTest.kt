package com.luisroman.userzoom.behaviors.login

import com.luisroman.userzoom.page.login.LoginPage
import com.luisroman.userzoom.page.main.ProjectDashboardPage
import com.luisroman.seleniumbase.utils.SeleniumTest
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.platform.runner.JUnitPlatform
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.junit.jupiter.SpringExtension

@SeleniumTest
@RunWith(JUnitPlatform::class)
@ExtendWith(SpringExtension::class)
@DisplayName("Login Successful Test")
open class LoginSuccessfulTest {

    @Autowired
    lateinit var loginPage: LoginPage

    @Autowired
    lateinit var projectDashboardPage: ProjectDashboardPage

    @Test
    fun `Do a successful Login`() {
        loginPage.login(EMAIL, PASSWORD)
        assertTrue(projectDashboardPage.isProjectDashboardDisplayed())
    }

    companion object {
        private const val EMAIL = "luisrmn90@gmail.com"
        private const val PASSWORD = "Testing2019"
    }

}