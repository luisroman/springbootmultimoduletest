package com.luisroman.seleniumbase.spring

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan("com.luisroman")
open class SpringConfiguration {

    companion object {
        fun main(args: Array<String>) {
            SpringApplication.run(SpringConfiguration::class.java, *args)
        }
    }
}