package com.luisroman.seleniumbase.config

import org.openqa.selenium.Dimension
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.edge.EdgeDriver
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.ie.InternetExplorerDriver
import org.openqa.selenium.ie.InternetExplorerOptions
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import java.net.URL
import java.util.Locale
import java.util.concurrent.TimeUnit
import javax.annotation.PreDestroy

@Configuration
open class SeleniumSetUp {

    private lateinit var mainDriver: WebDriver

    private var gridAddress: String = "http://172.0.0.1:4444/wd/hub"

    @Value("\${url.mainurl}")
    private lateinit var mainUrl: String

    @Bean
    @Profile("Chrome")
    open fun chromeDriver(): WebDriver {
        return buildRemoteWebDriver(Browser.CHROME)
    }

    @Bean
    @Profile("Firefox")
    open fun firefoxDriver(): WebDriver {
        return buildRemoteWebDriver(Browser.FIREFOX)
    }

    @Bean
    @Profile("Edge")
    open fun edgeDriver(): WebDriver {
        return buildRemoteWebDriver(Browser.EDGE)
    }

    @Bean
    @Profile("IE")
    open fun internetExplorerDriver(): WebDriver {
        return buildRemoteWebDriver(Browser.IE)
    }

    @Bean
    @Profile("GridFirefox")
    open fun gridFirefoxDriver(): WebDriver {
        return buildRemoteWebDriver(Browser.GRID_FIREFOX)
    }

    @Bean
    @Profile("GridChrome")
    open fun gridChromeDriver(): WebDriver {
        return buildRemoteWebDriver(Browser.GRID_CHROME)
    }

    @Bean
    @Profile("GridEdge")
    open fun gridEdgeDriver(): WebDriver {
        return buildRemoteWebDriver(Browser.GRID_EDGE)
    }

    @Bean
    @Profile("GridIE")
    open fun gridIEDriver(): WebDriver {
        return buildRemoteWebDriver(Browser.GRID_IE)
    }

    @PreDestroy
    private fun quit() {
        mainDriver.quit()
    }

    private fun buildRemoteWebDriver(desiredBrowser: Browser): WebDriver {
        if (System.getProperty("GRID_ADDRESS") != null) {
            gridAddress = System.getProperty("GRID_ADDRESS")
        }
        when(desiredBrowser) {
            Browser.CHROME -> {
                System.setProperty("webdriver.chrome.driver", getChromeDriverPath())
                val chromeOptions = ChromeOptions()
                mainDriver = ChromeDriver(chromeOptions)
            }
            Browser.FIREFOX -> {
                System.setProperty("webdriver.gecko.driver", getGeckoDriverPath())
                val firefoxOptions = FirefoxOptions()
                firefoxOptions.setAcceptInsecureCerts(true)
                mainDriver = FirefoxDriver(firefoxOptions)
            }
            Browser.EDGE -> {
                System.setProperty("webdriver.edge.driver", getEdgeDriverPath())
                mainDriver = EdgeDriver()
            }
            Browser.IE -> {
                val path = if (System.getProperty("IE32DRIVER") == null) {
                    getInternetExplorer64DriverPath()
                } else {
                    getInternetExplorer32DriverPath()
                }
                System.setProperty("webdriver.ie.driver", path)
                val internetExplorerOptions = InternetExplorerOptions()
                internetExplorerOptions.introduceFlakinessByIgnoringSecurityDomains()
                internetExplorerOptions.requireWindowFocus()
                mainDriver = InternetExplorerDriver(internetExplorerOptions)
            }
            Browser.GRID_CHROME -> {
                mainDriver = RemoteWebDriver(URL(gridAddress), DesiredCapabilities.chrome())
            }
            Browser.GRID_FIREFOX -> {
                mainDriver = RemoteWebDriver(URL(gridAddress), DesiredCapabilities.firefox())
            }
            Browser.GRID_EDGE -> {
                mainDriver = RemoteWebDriver(URL(gridAddress), DesiredCapabilities.edge())
            }
            Browser.GRID_IE -> {
                val internetExplorerGridOptions = InternetExplorerOptions()
                internetExplorerGridOptions.introduceFlakinessByIgnoringSecurityDomains()
                internetExplorerGridOptions.requireWindowFocus()
                mainDriver = RemoteWebDriver(URL(gridAddress), internetExplorerGridOptions)
            }
        }
        mainDriver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT, TimeUnit.SECONDS)
        mainDriver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS)
        mainDriver.manage().window().size = Dimension(1024, 768)
        mainDriver.get(mainUrl)
        return mainDriver
    }

    private fun getChromeDriverPath(): String {
        val oS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH)
        return if (oS.contains("mac") || oS.contains("darwin")) {
            getResource("/drivers/chromedriver-mac")
        } else if (oS.contains("win")) {
            getResource("/drivers/chromedriver-win32.exe")
        } else {
            getResource("/drivers/chromedriver-linux64")
        }
    }

    private fun getGeckoDriverPath(): String {
        val oS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH)
        return if (oS.contains("mac") || oS.contains("darwin")) {
            getResource("/drivers/geckodriver-mac")
        } else if (oS.contains("win")) {
            getResource("/drivers/geckodriver-win64.exe")
        } else {
            getResource("/drivers/geckodriver-linux64")
        }
    }

    private fun getInternetExplorer64DriverPath(): String {
        return getResource("/drivers/IEDriverServer-64.exe")
    }

    private fun getInternetExplorer32DriverPath(): String {
        return getResource("/drivers/IEDriverServer-32.exe")
    }

    private fun getEdgeDriverPath(): String {
        return getResource("/drivers/MicrosoftWebDriver.exe")
    }

    private enum class Browser {
        CHROME, FIREFOX, IE, EDGE, GRID_FIREFOX, GRID_CHROME, GRID_IE, GRID_EDGE
    }

    private fun getResource(path: String): String {
        var value = SeleniumSetUp::class.java.getResource(path).path.replace("%20".toRegex(), " ")
        if (value.contains("file:")) {
            value = System.getProperty("user.dir").replace("%20".toRegex(), " ").substringBeforeLast("/") + "/baseproject/src/main/resources" + path
        }
        return value
    }


    companion object {
        private const val IMPLICITLY_WAIT = 10L
        private const val PAGE_LOAD_TIMEOUT = 10L
    }

}