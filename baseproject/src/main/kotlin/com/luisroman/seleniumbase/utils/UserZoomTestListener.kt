package com.luisroman.seleniumbase.utils

import org.springframework.stereotype.Component
import org.springframework.test.context.TestContext
import org.springframework.test.context.support.DirtiesContextTestExecutionListener

@Component
class UserZoomTestListener : DirtiesContextTestExecutionListener() {

    @Throws(Exception::class)
    override fun afterTestMethod(testContext: TestContext) {
        testContext.applicationContext.getBean(ScreenshotUtils::class.java).makeScreenshot()
        super.afterTestMethod(testContext)
    }

}