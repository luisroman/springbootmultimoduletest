package com.luisroman.seleniumbase.utils

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
open class ElementUtils {

    @Autowired
    private lateinit var driver: WebDriver

    fun waitForClickable(element: WebElement) {
        val wait = WebDriverWait(driver, IMPLICITLY_WAIT)
        wait.until(ExpectedConditions.elementToBeClickable(element))
    }

    companion object {
        private const val IMPLICITLY_WAIT = 20L
    }

}