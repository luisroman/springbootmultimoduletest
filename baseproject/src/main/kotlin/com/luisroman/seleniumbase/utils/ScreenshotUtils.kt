package com.luisroman.seleniumbase.utils

import io.qameta.allure.Attachment
import org.openqa.selenium.OutputType
import org.openqa.selenium.TakesScreenshot
import org.openqa.selenium.WebDriver
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
open class ScreenshotUtils {

    @Autowired
    private lateinit var driver: WebDriver

    @Attachment("Test Screenshot")
    fun makeScreenshot(): ByteArray {
        return (driver as TakesScreenshot).getScreenshotAs(OutputType.BYTES)
    }
}