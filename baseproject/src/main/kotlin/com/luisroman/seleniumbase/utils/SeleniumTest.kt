package com.luisroman.seleniumbase.utils

import com.luisroman.seleniumbase.spring.SpringConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener

@SpringBootTest(classes = [SpringConfiguration::class])
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestExecutionListeners(DependencyInjectionTestExecutionListener::class, UserZoomTestListener::class)
annotation class SeleniumTest